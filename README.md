# Tabs for Layout Builder

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ws_lb_tabs).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ws_lb_tabs).


## Requirements

This module requires the following modules:

- [Paragraphs](https://www.drupal.org/project/paragraphs)
- [Entity Reference Revision](https://www.drupal.org/project/entity_reference_revisions)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.


## Maintainers

- Andrii Podanenko - [podarok](https://www.drupal.org/u/podarok)
- Anatolii Poliakov - [anpolimus](https://www.drupal.org/u/anpolimus)
- Dima Danylevskyi - [danylevskyi](https://www.drupal.org/u/danylevskyi)
- Max - [mkdok](https://www.drupal.org/u/mkdok)
- Open-Y-Distro - [Open-Y-Distro](https://www.drupal.org/u/open-y-distro)
- Andrey Zbukar - [andrey_zb](https://www.drupal.org/u/andrey_zb)
